import React, {useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  YellowBox,
  Keyboard,
  FlatList,
  Image,
} from 'react-native';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import {Input} from 'react-native-elements';
import DateTimePicker from 'react-native-modal-datetime-picker';
import {ScrollView} from 'react-native-gesture-handler';

function AutoCompleteItem({name, onTouch}) {
  return (
    <TouchableOpacity
      onPress={() => {
        onTouch();
      }}>
      <Text style={styles.autoCompleteHint}>{name}</Text>
    </TouchableOpacity>
  );
}

function Home({navigation}) {
  var currentDate = new Date().toLocaleDateString();
  var currenttime = new Date().toLocaleTimeString().substring(0, 5);
  const [departuresHint, setDeparturesHint] = useState([]);
  const [arrivalsHint, setArrivalsHint] = useState([]);
  const [departure, setDeparture] = useState('');
  const [destination, setDestination] = useState('');
  const [modeShow, setModeShow] = useState('date');
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [dat, setdat] = useState(currentDate);
  const [time, setTime] = useState(currenttime);
  const [dbResult, setDbResult] = useState([]);
  const [statusCode, setStatusCode] = useState(0);

  const showDatePicker = () => {
    setModeShow('date');
    setDatePickerVisibility(true);
  };

  const showTimePicker = () => {
    setModeShow('time');
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const handleConfirm = date => {
    Keyboard.dismiss();
    hideDatePicker();
    if (modeShow === 'date') {
      setdat(date.toLocaleDateString());
    } else {
      setTime(date.toLocaleTimeString().substring(0, 5));
    }
  };

  return (
    <ScrollView style={{backgroundColor: '#ffffff', flex: 1}}>
      <View style={{padding: 16, backgroundColor: 'white', flex: 1}}>
        <View style={{flex: 1}}>
          <View style={{flex: 1, alignItems: 'center'}}>
            <Image style={styles.img} source={require('./logo.png')} />
          </View>
          <View style={{flex: 4}}>
            <View style={styles.autoCompleteContainer}>
              <Input
                label="Select a station"
                value={departure}
                placeholder="From"
                onChangeText={text => {
                  setDeparture(text);
                  fetch(
                    'http://transport.opendata.ch/v1/locations?type=station&query=' +
                      text,
                  )
                    .then(response => response.json())
                    .then(json => setDeparturesHint(json.stations))
                    .catch(error => console.error(error));
                }}
                leftIcon=<FontAwesomeIcon name="train" size={24} color="gray" />
              />
              <FlatList
                style={styles.autoComplete}
                data={departuresHint}
                keyExtractor={item => {
                  return Math.random().toString();
                }}
                renderItem={({item}) => (
                  //render every item as an AutoCompleteComponent
                  <AutoCompleteItem
                    name={item.name}
                    onTouch={() => {
                      setDeparture(item.name);
                      setDeparturesHint([]);
                    }}
                  />
                )}
              />
            </View>
            <View style={styles.autoCompleteContainer}>
              <Input
                label="Select a station"
                value={destination}
                placeholder="To"
                onChangeText={text => {
                  setDestination(text);
                  fetch(
                    'http://transport.opendata.ch/v1/locations?type=station&query=' +
                      text,
                  )
                    .then(response => response.json())
                    .then(json => setArrivalsHint(json.stations))
                    .catch(error => console.error(error));
                }}
                leftIcon=<FontAwesomeIcon name="train" size={24} color="gray" />
              />
              <FlatList
                style={styles.autoComplete}
                data={arrivalsHint}
                keyExtractor={item => {
                  return Math.random().toString();
                }}
                renderItem={({item}) => (
                  //render every item as an AutoCompleteComponent
                  <AutoCompleteItem
                    name={item.name}
                    onTouch={() => {
                      setDestination(item.name);
                      setArrivalsHint([]);
                    }}
                  />
                )}
              />
            </View>
            <Input
              label="select a Date"
              placeholder={currentDate}
              defaultValue={dat}
              onFocus={showDatePicker}
              leftIcon=<FontAwesomeIcon
                name="calendar"
                size={24}
                color="gray"
              />
            />
            <Input
              label="select a time"
              placeholder={currenttime}
              defaultValue={time}
              onFocus={showTimePicker}
              leftIcon=<FontAwesomeIcon name="clock-o" size={24} color="gray" />
            />
            <TouchableOpacity
              style={styles.submitButton}
              onPress={() =>
                fetch(
                  'http://transport.opendata.ch/v1/connections?from=' +
                    departure +
                    '&to=' +
                    destination +
                    '&date=' +
                    dat +
                    '&time=' +
                    time,
                )
                  .then(response => {
                    const statusCode2 = response.status;
                    setStatusCode(statusCode2);
                    return response.json();
                  })
                  .then(json => setDbResult(json.connections))
                  .catch(error => console.log(error))
                  .finally(() => {
                    navigation.navigate('results', {
                      departure: {departure},
                      destination: {destination},
                      dat: {dat},
                      time: {time},
                      dbResult: {dbResult},
                      statusCode: {statusCode},
                    });
                  })
              }>
              <Text style={styles.submitButtonText}>Find a train</Text>
            </TouchableOpacity>
            <DateTimePicker
              isVisible={isDatePickerVisible}
              mode={modeShow}
              onConfirm={handleConfirm}
              onCancel={hideDatePicker}
            />
          </View>
        </View>
      </View>
    </ScrollView>
  );
}

YellowBox.ignoreWarnings([
  'VirtualizedLists should never be nested', // TODO: Remove when fixed
]);

const styles = StyleSheet.create({
  engine: {
    position: 'absolute',
    right: 0,
  },
  container: {
    paddingTop: 23,
  },
  input: {
    //margin: 15,
    marginBottom: 15,
    height: 40,
    borderColor: '#1E90FF',
    borderWidth: 1,
  },
  submitButton: {
    backgroundColor: '#1E90FF',
    padding: 10,
    margin: 15,
    height: 40,
    elevation: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  submitButtonText: {
    color: 'white',
    fontSize: 24,
    textAlign: 'center',
  },
  label: {
    fontWeight: 'bold',
    fontSize: 15,
    color: 'gray',
    marginBottom: 4,
  },
  autoComplete: {
    margin: 0,
    marginLeft: 20,
    marginRight: 20,
    maxHeight: 350,
    overflow: 'scroll',
    borderRadius: 5,
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 2,
    position: 'absolute',
    width: '90%',
    top: 80,
    backgroundColor: 'white',
    zIndex: 1,
  },
  autoCompleteContainer: {
    position: 'relative',
  },
  autoCompleteHint: {
    color: 'gray',
    padding: 16,
    fontSize: 16,
    justifyContent: 'center',
  },
  img: {
    maxWidth: 200,
    margin: 20,
    resizeMode: 'center',
  },
});

export default Home;
