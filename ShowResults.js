import React, {useState} from 'react';
import {
  Alert,
  StyleSheet,
  View,
  FlatList,
  Text,
  TouchableOpacity,
  TouchableHighlight,
  Modal,
} from 'react-native';

function Stop({stationName, platform, arrival, departure, delay}) {
  return (
    <TouchableOpacity style={styles.stop}>
      <View style={styles.stopRow}>
        <Text style={styles.stopLabel}>Station: </Text>
        <Text>{stationName}</Text>
      </View>
      <View style={platform ? styles.stopRow : styles.hideStopFiled}>
        <Text style={platform ? styles.stopLabel : styles.hideStopFiled}>
          Platform:{' '}
        </Text>
        <Text style={platform ? {} : styles.hideStopFiled}>{platform}</Text>
      </View>
      <View style={arrival ? styles.stopRow : styles.hideStopFiled}>
        <Text style={arrival ? styles.stopLabel : styles.hideStopFiled}>
          Arrival:{' '}
        </Text>
        <Text style={arrival ? {} : styles.hideStopFiled}>{arrival}</Text>
      </View>
      <View style={departure ? styles.stopRow : styles.hideStopFiled}>
        <Text style={departure ? styles.stopLabel : styles.hideStopFiled}>
          Departure:{' '}
        </Text>
        <Text style={departure ? {} : styles.hideStopFiled}>{departure}</Text>
      </View>
      <View style={styles.stopRow}>
        <Text style={styles.stopLabel}>Delay: </Text>
        <Text>{delay ? delay : 'train is on time'}</Text>
      </View>
    </TouchableOpacity>
  );
}
/* Used to render a travel solution item inside a flat list */
function Travel({departure, arrival, stops, onTouch}) {
  const style = {
    backgroundColor: 'white',
    marginBottom: 8,
    padding: 8,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  };
  return (
    <TouchableOpacity style={style} onPress={() => onTouch()}>
      <View style={departure ? styles.stopRow : styles.hideStopFiled}>
        <Text style={departure ? styles.stopLabel : styles.hideStopFiled}>
          Departure:{' '}
        </Text>
        <Text style={departure ? {} : styles.hideStopFiled}>{departure}</Text>
      </View>
      <View style={arrival ? styles.stopRow : styles.hideStopFiled}>
        <Text style={arrival ? styles.stopLabel : styles.hideStopFiled}>
          Arrival:{' '}
        </Text>
        <Text style={arrival ? {} : styles.hideStopFiled}>{arrival}</Text>
      </View>
      <View style={styles.stopRow}>
        <Text style={styles.stopLabel}>Stops: </Text>
        <Text>{stops === 2 ? 'direct' : stops}</Text>
      </View>
    </TouchableOpacity>
  );
}

const ShowModalStops = ({
  passList,
  modalVisible,
  setModalVisible,
  departure,
  destination,
}) => {
  return (
    <View style={styles.centeredView}>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={{fontWeight: 'bold', color: 'gray'}}>Your travel</Text>
            <Text style={{marginBottom: 16}}>
              From: {departure}
              {'\n'}
              To: {destination}
              {'\n'}
              Number of total stops: {passList.length}
            </Text>
            <FlatList
              data={passList}
              renderItem={({item}) => (
                //render every item as a Stop component
                <Stop
                  stationName={item.station.name}
                  platform={item.platform}
                  arrival={item.arrival}
                  departure={item.departure}
                  delay={item.delay}
                />
              )}
              keyExtractor={item => item.station.id}
            />

            <TouchableHighlight
              style={{
                ...styles.openButton,
                backgroundColor: '#2196F3',
                marginTop: 16,
              }}
              onPress={() => {
                setModalVisible(!modalVisible);
              }}>
              <Text style={styles.textStyle}>Close</Text>
            </TouchableHighlight>
          </View>
        </View>
      </Modal>
    </View>
  );
};

function ShowResults({route}) {
  const [data, setData] = useState([]);
  const [modalVisible, setModalVisible] = useState(false);
  const [passList, setPassList] = useState([]);
  const [err, setErr] = useState([]);
  const {departure} = route.params.departure;
  const {destination} = route.params.destination;
  const {dat} = route.params.dat;
  const {time} = route.params.time;
  const {dbResult} = route.params.dbResult;
  const {statusCode} = route.params.statusCode;

  return (
    <View style={styles.centeredView2}>
      {statusCode === 200 ? (
        <FlatList
          data={dbResult}
          keyExtractor={item => {
            return Math.random().toString();
          }}
          renderItem={({item}) => (
            //render every item as aP> Travel component
            <Travel
              departure={item.from.departure}
              arrival={item.to.arrival}
              stops={item.sections[0].journey.passList.length}
              onTouch={() => {
                setPassList(item.sections[0].journey.passList);
                setModalVisible(true);
              }}
            />
          )}
        />
      ) : (
        <Text style={{justifyContent: 'center', alignItems: 'center'}}>
          System error
        </Text>
      )}

      <ShowModalStops
        passList={passList}
        modalVisible={modalVisible}
        setModalVisible={setModalVisible}
        departure={departure}
        destination={destination}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 8,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
    marginBottom: 22,
  },
  centeredView2: {
    flex: 1,
    justifyContent: 'center',
    padding: 16,
  },
  openButton: {
    backgroundColor: '#F194FF',
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    color: 'black',
  },
  stopRow: {
    flexDirection: 'row',
    marginBottom: 5,
  },
  stopLabel: {
    fontWeight: 'bold',
  },
  hideStopFiled: {
    display: 'none',
  },
  stop: {
    backgroundColor: 'lightgray',
    borderRadius: 10,
    borderColor: 'black',
    borderWidth: 1,
    marginBottom: 8,
    padding: 8,
  },
});

export default ShowResults;
